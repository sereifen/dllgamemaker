﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllGameMaker;
using Object = DllGameMaker.Object;

namespace BuscaMinas
{
    /// <summary>
    /// Enum of all types of casilla
    /// </summary>
    public enum CasillaType
    {
        None,
        Mina,
        Numero
    };

    /// <summary>
    /// enum of status of this casilla
    /// </summary>
    public enum CasillaStatus
    {
        Open,
        Closed,
        Marked
    };

    class Casilla : Object
    {
        private CasillaType _type;

        /// <summary>
        /// Type of this casilla
        /// </summary>
        public CasillaType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                SetColors();
            }
        }

        /// <summary>
        /// Status of this casilla
        /// </summary>
        public CasillaStatus Status { get; set; }

        /// <summary>
        /// Number of this casilla
        /// </summary>
        public int Number { get; private set; }

        private const int Colorclosed = 190;
        private const int ColorOpen = 240;
        private const int ColorMark = 90;
        private const int ColorMina = 0;
        private const int ColorMum = 0;

        /// <summary>
        /// Constructor with the position of object
        /// </summary>
        /// <param name="inix">position of x where start</param>
        /// <param name="iniy">position of y where start</param>
        /// <param name="endx">position of x where end</param>
        /// <param name="endy">position of y where end</param>
        /// <param name="type">Type of casilla default to none</param>
        public Casilla(double inix, double iniy, double endx, double endy, CasillaType type = CasillaType.None) : base()
        {
            Vertex = new List<Point2D>
            {
                new Point2D(inix, iniy), new Point2D(endx, iniy), new Point2D(endx, endy), new Point2D(inix, endy)
            };
            Number = 0;
            Type = type;
            Status = CasillaStatus.Closed;
        }

        /// <summary>
        /// constructor for make duplicate
        /// </summary>
        /// <param name="other">other casilla for clone</param>
        public Casilla(Casilla other):base(other)
        {
            _type = other.Type;
            Status = other.Status;
            Number = other.Number;
        }

        /// <summary>
        /// Override of Duplicate for duplicate object
        /// </summary>
        /// <returns>new object with the same parameters</returns>
        public override Object Duplicate()
        {
            return new Casilla(this);
        }

        public void AddNumber()
        {
            if (Type== CasillaType.Mina)return;
            if (Number==0) Type = CasillaType.Numero;
            Number++;
        }
        
        /// <summary>
        /// set colors to the object by the default settings
        /// </summary>
        private void SetColors()
        {
            switch (Status)
            {
                case CasillaStatus.Open:
                    switch (Type)
                    {
                        case CasillaType.None:
                            Red = ColorOpen;
                            Blue = ColorOpen;
                            Green = ColorOpen;
                            break;
                        case CasillaType.Mina:
                            Red = ColorMina;
                            Blue = ColorMina;
                            Green = ColorMina;
                            break;
                        case CasillaType.Numero:
                            Red = ColorMum;
                            Blue = ColorMum;
                            Green = ColorMum;
                            break;
                    }
                    break;
                case CasillaStatus.Closed:
                    Red = Colorclosed;
                    Blue = Colorclosed;
                    Green = Colorclosed;
                    break;
                case CasillaStatus.Marked:
                    Red = ColorMark;
                    Blue = ColorMark;
                    Green = ColorMark;
                    break;
            }
        }
    }
}
