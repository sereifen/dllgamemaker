﻿using System;
using System.Collections;
using System.Collections.Generic;
using DllGameMaker;

namespace BuscaMinas
{
    class Mapa
    {
        /// <summary>
        /// Size of map on X
        /// </summary>
        public int X { get; }

        /// <summary>
        /// Size of map on Y
        /// </summary>
        public int Y { get; }

        private Dictionary<Tuple<int, int>, Guid> _casillas;

        private ScreenControler _doc;

        /// <summary>
        /// Constructor with number of casillas total
        /// </summary>
        /// <param name="number">number of casillas</param>
        /// <param name="mines">number of mines</param>
        /// <param name="doc">Document for add objects and replace it</param>
        public Mapa(int number,int mines,ScreenControler doc)
        {
            _doc = doc;
            _casillas = new Dictionary<Tuple<int, int>, Guid>();
            X = (int)Math.Sqrt(number);
            Y = number/X;
            var sizeX = 2 / (X * 1.5);
            var sizeY = 2 / (Y * 1.5);
            var posmines = SetMines(mines);
            var map =  new List<List<Casilla>>();
            for (var i = 0; i < X; i++)
            {
                map.Add(new List<Casilla>());
                for (var j = 0; j < Y; j++)
                {
                    var posinix = ((sizeX / 2) + sizeX * i)-1;
                    var posiniy = ((sizeY / 2) + sizeY * j)-1;
                    var cas = new Casilla(posinix, posiniy, posinix+ sizeX, posiniy+ sizeY, posmines[i][j]?CasillaType.Mina : CasillaType.None);
                    map[i].Add(cas);
                }
            }
            ComputeNumbers(map, posmines);
            for (var i = 0; i < map.Count; i++)
            {
                for (var j = 0; j < map[i].Count; j++)
                {
                    var guid = doc.AddToDoc(map[i][j]);
                    _casillas.Add(new Tuple<int, int>(i,j),guid );
                }
            }
        }

        /// <summary>
        /// translate the double inside the screen to postion in map
        /// </summary>
        /// <param name="x">position of mouse</param>
        /// <param name="y">position of mouse</param>
        /// <param name="posx">position on map</param>
        /// <param name="posy">position on map</param>
        private void TranslatePosition(double x, double y, out int posx, out int posy)
        {
            var sizeX = 2 / (X * 1.5);
            var sizeY = 2 / (Y * 1.5);
            posx = (int)(((x + 1) - (sizeX / 2)) / sizeX);
            posy = (int)(((y + 1) - (sizeY / 2)) / sizeY);
        }

        /// <summary>
        /// Open the selected casilla and return true if is mina
        /// </summary>
        /// <param name="x">position on windows of mouse</param>
        /// <param name="y">position on windows of mouse</param>
        /// <returns>return true if is mina</returns>
        public bool OpenCasilla(int x, int y)
        {
            int posx, posy;
            TranslatePosition(x, y, out posx, out posy);
            var guid = _casillas[new Tuple<int, int>(posx, posy)];
            var obj = _doc.Find(guid) as Casilla;
            if (obj == null) return false;
            obj.Status= CasillaStatus.Open;
            _doc.Replace(guid, obj);
            return obj?.Type == CasillaType.Mina;
        }

        /// <summary>
        /// Add numbers to a casillas near to a mina
        /// </summary>
        /// <param name="map">map of all casillas</param>
        /// <param name="posmines">position of mines</param>
        private void ComputeNumbers(List<List<Casilla>> map, List<BitArray> posmines)
        {
            for (int i = 0; i < posmines.Count; i++)
            {
                for (int j = 0; j < posmines[i].Length; j++)
                {
                    if (!posmines[i][j])continue;
                    ComputeNumber(map,i,j);
                }
            }
        }

        /// <summary>
        /// add number to a casilla near
        /// </summary>
        /// <param name="map">map of all casillas</param>
        /// <param name="x">position of mina</param>
        /// <param name="y">position of mina</param>
        private void ComputeNumber(List<List<Casilla>> map, int x, int y)
        {
            for (int i = x-1; i != x+1; i++)
            {
                for (int j = y-1; j != y + 1; j++)
                {
                    if (i<0 || j<0) continue;
                    if (i>=X || j>=Y) continue;
                    map[i][j].AddNumber();
                }
            }
        }

        /// <summary>
        /// calculate the position of mines 
        /// </summary>
        /// <param name="mines">number of mines to set on map</param>
        /// <returns>position of mines on map</returns>
        private List<BitArray> SetMines(int mines)
        {
            var rnd = new Random();
            var result = new List<BitArray>();
            for (int i = 0; i < X; i++)
            {
                result.Add(new BitArray(Y));
            }
            for (int i = 0; i < mines; i++)
            {
                var x = rnd.Next(0, X);
                var y = rnd.Next(0, Y);
                if (result[x][y]) i--;
                else
                    result[x][y] = true;
            }
            return result;
        }
    }
}
