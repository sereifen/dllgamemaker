﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Graphics.OpenGL;

namespace DllGameMaker
{
    /// <summary>
    /// Class from all objects 
    /// </summary>
    public class Object
    {
        /// <summary>
        /// List of vertex for draw
        /// </summary>
        public List<Point2D> Vertex { get; set; }

        /// <summary>
        /// Color Red of object
        /// </summary>
        public double Red { get; set; }

        /// <summary>
        /// Color Blue of object
        /// </summary>
        public double Blue { get; set; }

        /// <summary>
        /// Color Green of object
        /// </summary>
        public double Green { get; set; }

        /// <summary>
        /// Unique Guid of object
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// visibility of object
        /// </summary>
        internal bool Visible { get; set; }

        /// <summary>
        /// Public Constructor of object
        /// </summary>
        public Object()
        {
            Visible = true;
            Blue = 0;
            Green = 0;
            Red = 0;
            Vertex = new List<Point2D>();
        }

        /// <summary>
        /// constructor with other object
        /// </summary>
        /// <param name="other">object to duplicate</param>
        public Object(Object other)
        {
            Vertex = other.Vertex.Select(d => new Point2D(d)).ToList();
            Blue = other.Blue;
            Green = other.Green;
            Red = other.Red;
            Visible = other.Visible;
            Id = other.Id;
        }
        
        /// <summary>
        /// Function for clone objects
        /// </summary>
        /// <returns>object cloned</returns>
        public virtual Object Duplicate()
        {
            return new Object(this);
        }

        /// <summary>
        /// virtual draw of object
        /// </summary>
        public virtual void Draw()
        {
            if (!Visible)return;
            foreach (var point2D in Vertex)
            {
                GL.Color3(Red, Green, Blue);
                GL.Vertex2(point2D.X, point2D.Y);
            }
        }
    }
}
